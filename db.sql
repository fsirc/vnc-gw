CREATE TABLE IF NOT EXISTS vnc_sessions (
    id integer primary key autoincrement,
    dst_addr varchar(100) not null,
    dst_port integer not null,
    vnc_pwd varchar(8) unique,
    single_use boolean not null default false,
    expiry bigint not null
);
