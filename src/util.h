#include <stdint.h>
#ifndef H_UTIL
#define H_UTIL
void util_bhtonl(uint32_t in, char* out);
void util_bhtons(uint16_t in, char* out);
uint32_t util_bntohl(char* in);
#endif
