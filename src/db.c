#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <libcutil/array.h>
#include "des/d3des.h"
#include "db.h"

#define FIND_VSES_SQL "SELECT id, dst_addr, dst_port, vnc_pwd, single_use, expiry FROM vnc_sessions"
#define INSERT_VSES_SQL "INSERT INTO vnc_sessions(dst_addr, dst_port, vnc_pwd, single_use, expiry) VALUES (?, ?, ?, ?, ?)"
#define DEL_VSES_SQL "DELETE FROM vnc_sessions WHERE id = ?"

sqlite3* db_open(char* db_path, char* sqlf_path) {
    sqlite3* db = NULL;
    char* buf = NULL;
    int sql_fd = -1;
    struct stat sti = {0};
    if(sqlite3_open(db_path, &db) || (sql_fd = open(sqlf_path, O_RDONLY)) < 0)
        return NULL;
    if(stat(sqlf_path, &sti) != 0)
        return NULL;
    if(!(buf = malloc(sti.st_size+1)))
        return NULL;
    if(read(sql_fd, buf, sti.st_size) < sti.st_size)
        return NULL;
    buf[sti.st_size] = '\0';
    close(sql_fd);
    if(sqlite3_exec(db, buf, NULL, NULL, NULL))
        return NULL;
    free(buf);
    return db;
}

short db_insert_vses_entry(sqlite3* dbc, struct db_vnc_ses_model* vmodel) {
    sqlite3_stmt* stmt = NULL;
    int r = -1;
    if(sqlite3_prepare_v2(dbc, INSERT_VSES_SQL, -1, &stmt, NULL))
        return 0;
    sqlite3_bind_text(stmt, 1, vmodel->dst_addr, -1, NULL);
    sqlite3_bind_int(stmt, 2, vmodel->dst_port);
    sqlite3_bind_text(stmt, 3, vmodel->vnc_pwd, -1, NULL);
    sqlite3_bind_int(stmt, 4, vmodel->single_use);
    sqlite3_bind_int64(stmt, 5, vmodel->expiry);
    r = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    if(r != SQLITE_DONE)
        return (r == SQLITE_CONSTRAINT ? 2 : 0);
    vmodel->id = (int)sqlite3_last_insert_rowid(dbc);
    return 1;
}

short db_del_vses(sqlite3* dbc, uint32_t vses_id) {
    sqlite3_stmt* stmt = NULL;
    int r = -1;
    if(sqlite3_prepare_v2(dbc, DEL_VSES_SQL, -1, &stmt, NULL))
        return 0;
    sqlite3_bind_int(stmt, 1, vses_id);
    r = sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    if(r != SQLITE_DONE)
        return 0;
    return 1;
}

array_t* db_lookup_all_vnc_sessions(sqlite3* dbc) {
    sqlite3_stmt* stmt = NULL;
    struct db_vnc_ses_model* vmodel = NULL;
    array_t* vmodels = array_new();
    if(!vmodels || sqlite3_prepare_v2(dbc, FIND_VSES_SQL, -1, &stmt, NULL))
        return NULL;
    while(sqlite3_step(stmt) == SQLITE_ROW) {
        vmodel = malloc(sizeof(struct db_vnc_ses_model));
        vmodel->id = sqlite3_column_int(stmt, 0);
        strncpy(vmodel->dst_addr, (char*)sqlite3_column_text(stmt, 1),
                sizeof(vmodel->dst_addr));
        vmodel->dst_port = sqlite3_column_int(stmt, 2);
        strncpy(vmodel->vnc_pwd,
                (char*)sqlite3_column_text(stmt, 3), sizeof(vmodel->vnc_pwd));
        vmodel->single_use = sqlite3_column_int(stmt, 4);
        vmodel->expiry = sqlite3_column_int64(stmt, 5);
        array_insert(vmodels, vmodel);
    }
    sqlite3_finalize(stmt);
    return vmodels;
}

struct db_vnc_ses_model* db_find_vses_entry(sqlite3* dbc,
        char* challenge, char* client_challenge) {
    struct db_vnc_ses_model* vmodel = NULL;
    sqlite3_stmt* stmt = NULL;
    char dec[8], pwd[8] = {0};
    if(sqlite3_prepare_v2(dbc, FIND_VSES_SQL, -1, &stmt, NULL))
        return NULL;
    while(sqlite3_step(stmt) == SQLITE_ROW) {
        if(!sqlite3_column_text(stmt, 3)) continue;
        strncpy(pwd, (char*)sqlite3_column_text(stmt, 3), 8);
        deskey((void*)pwd, EN0);
        des((void*)challenge, (void*)dec);
        if(!memcmp(client_challenge, dec, 8)) {
            // match found :D
            vmodel = malloc(sizeof(struct db_vnc_ses_model));
            vmodel->id = sqlite3_column_int(stmt, 0);
            strncpy(vmodel->dst_addr, (char*)sqlite3_column_text(stmt, 1),
                    sizeof(vmodel->dst_addr));
            vmodel->dst_port = sqlite3_column_int(stmt, 2);
            strncpy(vmodel->vnc_pwd, pwd, sizeof(vmodel->vnc_pwd));
            vmodel->single_use = sqlite3_column_int(stmt, 4);
            vmodel->expiry = sqlite3_column_int64(stmt, 5);
            break;
        }
    }
    sqlite3_finalize(stmt);
    return vmodel;
}
