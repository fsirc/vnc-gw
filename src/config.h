#include <stdint.h>
#ifndef H_CONFIG
#define H_CONFIG
struct config {
    uint16_t ln_port;
    uint16_t ln_admin_port;
    char* ln_host;
    char* admin_pw;
    char* log_path;
};
struct config* config_read(char* file_path);
void config_free(struct config** cfg);
#endif
