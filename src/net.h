#include <stdint.h>
#include <netinet/in.h>
#ifndef H_NET
#define H_NET
union net_sa_union {
    struct sockaddr_in v4;
    struct sockaddr_in6 v6;
};
struct net_sockaddr {
    short af;
    union net_sa_union sa;
};
#define NET_SOCKADDR_LEN(sa) (sa.af == AF_INET ? sizeof(struct sockaddr_in) : sizeof(struct sockaddr_in6))
#define NET_SOCKADDR_GET_ADDR(sockaddr) (sockaddr.af == AF_INET ? (void*)&sockaddr.sa.v4 : (void*)&sockaddr.sa.v6)
#define NET_SOCKADDR_GET_SADDR(sockaddr) (sockaddr.af == AF_INET ? (void*)&sockaddr.sa.v4.sin_addr.s_addr : (void*)&sockaddr.sa.v6.sin6_addr.s6_addr)
// af may be NULL
int net_new_ln(char* bind_host, uint16_t port, int* af);
int net_new_sock(char* addr, uint16_t port);
#endif
