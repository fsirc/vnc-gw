#include <stdint.h>
#include <sqlite3.h>
#include <time.h>
#include <libcutil/array.h>
#ifndef H_DB
#define H_DB
struct db_vnc_ses_model {
    uint32_t id;
    char dst_addr[100+1];
    uint16_t dst_port;
    char vnc_pwd[8+1];
    short single_use;
    time_t expiry;
};
sqlite3* db_open(char* db_path, char* sqlf_path);
struct db_vnc_ses_model* db_find_vses_entry(sqlite3* dbc,
        char* challenge, char* client_challenge);
array_t* db_lookup_all_vnc_sessions(sqlite3* dbc);
short db_insert_vses_entry(sqlite3* dbc, struct db_vnc_ses_model* vmodel);
short db_del_vses(sqlite3* dbc, uint32_t vses_id);
#endif
