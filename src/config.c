#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <libcutil/ini.h>
#include "config.h"

struct config* config_read(char* file_path) {
    struct config* cfg = NULL;
    ini_t* tcfg = NULL;
    int fd = -1;
    char* err = NULL, *s = NULL;
    if(!(cfg = malloc(sizeof(struct config))))
        return NULL;
    cfg->admin_pw = NULL;
    cfg->ln_host = NULL;
    cfg->log_path = NULL;
    if((fd = open(file_path, O_RDONLY)) < 0 || !(tcfg = ini_parse_fd(fd))) {
        err = "failed to open file";
        goto ret;
    }
    close(fd);
    if(!(s = ini_lookup_key(tcfg, "gateway", "ln_host", 0, 0))) {
        err = "invalid ln_host value";
        goto ret;
    }
    cfg->ln_host = strdup(s);
    if(!(s = ini_lookup_key(tcfg, "gateway", "admin_pw", 0, 0))) {
        err = "invalid admin_pw value";
        goto ret;
    }
    cfg->admin_pw = strdup(s);
    if((s = ini_lookup_key(tcfg, "gateway", "log_path", 0, 0))) {
        cfg->log_path = strdup(s);
    }
    if(!(s = ini_lookup_key(tcfg, "gateway", "ln_port", 0, 0)) || atoi(s) < 1) {
        err = "invalid ln_port value";
        goto ret;
    }
    cfg->ln_port = atoi(s);
    if(!(s = ini_lookup_key(tcfg, "gateway", "ln_admin_port", 0, 0)) || atoi(s) < 1) {
        err = "invalid ln_admin_port value";
        goto ret;
    }
    cfg->ln_admin_port = atoi(s);
ret:
    ini_free(tcfg);
    if(err) {
        config_free(&cfg);
        return NULL;
    }
    return cfg;
}

void config_free(struct config** cfg) {
    if((*cfg)->ln_host)
        free((*cfg)->ln_host);
    if((*cfg)->admin_pw)
        free((*cfg)->admin_pw);
    if((*cfg)->log_path)
        free((*cfg)->log_path);
    free(*cfg);
    *cfg = NULL;
}
