#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <libcutil/logger.h>
#include "evloop/evloop.h"
#include "net.h"
#include "admin.h"
#include "vnc.h"
#include "db.h"
#include "config.h"

int main(int argc, char** argv) {
    struct config* cfg = config_read("config.ini");
    struct evloop_ctx* evctx = NULL;
    struct vnc_ln_ctx lnctx = {0};
    struct admin_ln_ctx alnctx = {0};
    int lfd = -1, alfd = -1, af = -1;
    sqlite3* dbc = NULL;
    table_t* vnc_sessions = NULL;
    logger_t* logger = NULL;

    if(!cfg) {
        puts("[config] failed to read config");
        return 1;
    }
    if((lfd = net_new_ln(cfg->ln_host, cfg->ln_port, &af)) < 0) {
        puts("[gateway] failed to spawn listener");
        return 1;
    }
    if((alfd = net_new_ln(cfg->ln_host, cfg->ln_admin_port, NULL)) < 0) {
        puts("[gateway] failed to spawn admin listener");
        return 1;
    }
    if(!(dbc = db_open("db.db", "db.sql"))) {
        puts("[gateway] failed to open DB");
        return 1;
    }
    if(!(logger = logger_new(cfg->log_path, 1))) {
        puts("[gateway] failed to init logger");
        return 1;
    }
    srand(time(NULL));
    evloop_init();
    evctx = evloop_new_ctx();
    vnc_sessions = table_new();
    lnctx.vnc_sessions = vnc_sessions;
    lnctx.evctx = evctx;
    lnctx.dbc = dbc;
    lnctx.addr_family = af;
    lnctx.logger = logger;

    alnctx.vnc_sessions = vnc_sessions;
    alnctx.evctx = evctx;
    alnctx.admin_password = cfg->admin_pw;
    alnctx.start_time = time(NULL);
    alnctx.dbc = dbc;
    alnctx.addr_family = af;
    alnctx.logger = logger;

    evloop_new_fd_ev(evctx, NULL, NULL, (void*)vnc_ln_recv_cb, -1, &lnctx, lfd);
    evloop_new_fd_ev(evctx, NULL, NULL, (void*)admin_ln_recv_cb, -1, &alnctx, alfd);
    evloop_run(evctx);
    return 0;
}
