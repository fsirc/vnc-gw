#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <assert.h>
#include <arpa/inet.h>
#include <libcutil/table.h>
#include <libcutil/util.h>
#include "db.h"
#include "vnc.h"
#include "admin.h"

#define CONN_BANNER "VNCGateway\r\n"
#define CMD_NOT_FOUND_ERR "command not found"
#define CMD_AUTH_REQ_ERR "you must be authorised to use that command"
#define CMD_ARG_ERR "one or more arguments are either missing or are incorrect: %s"
static void admin_conn_recv_cb(struct evloop_fd_ev* ev, char* obuf,
                               int olen, struct admin_conn_ctx* ctx);
static void admin_cmd_uptime(struct admin_cmd_res* res, table_t* arg_tbl,
                             struct admin_conn_ctx* ctx);
static void admin_cmd_sessions(struct admin_cmd_res* res, table_t* arg_tbl,
                               struct admin_conn_ctx* ctx);
static void admin_cmd_pass(struct admin_cmd_res* res, table_t* arg_tbl,
                           struct admin_conn_ctx* ctx);
static void admin_cmd_hosts(struct admin_cmd_res* res, table_t* arg_tbl,
                            struct admin_conn_ctx* ctx);
static void admin_cmd_add_host(struct admin_cmd_res* res, table_t* arg_tbl,
                               struct admin_conn_ctx* ctx);
static void admin_cmd_del_host(struct admin_cmd_res* res, table_t* arg_tbl,
                               struct admin_conn_ctx* ctx);
static void admin_cmd_free_argv(table_t* args, struct admin_cmd* cmd);

static struct admin_cmd_arg admin_cmd_pass_args[] = {
    {"pass", 1, ACMD_ARG_STR},
    {NULL, 0, 0}
};
static struct admin_cmd_arg admin_cmd_add_host_args[] = {
    {"addr", 1, ACMD_ARG_STR},
    {"port", 1, ACMD_ARG_INT},
    {"password", 1, ACMD_ARG_STR},
    {"single_use", 1, ACMD_ARG_INT},
    {"expiration", 1, ACMD_ARG_INT},
    {NULL, 0, 0}
};
static struct admin_cmd_arg admin_cmd_del_host_args[] = {
    {"id", 1, ACMD_ARG_INT},
    {NULL, 0, 0}
};
static struct admin_cmd admin_cmds[] = {
    {"uptime", NULL, 0, admin_cmd_uptime},
    {"sessions", NULL, 1, admin_cmd_sessions},
    {"hosts", NULL, 1, admin_cmd_hosts},
    {"add_host", admin_cmd_add_host_args, 1, admin_cmd_add_host},
    {"del_host", admin_cmd_del_host_args, 1, admin_cmd_del_host},
    {"pass", admin_cmd_pass_args, 0, admin_cmd_pass},
    {NULL, 0, 0, NULL}
};

static void admin_cmd_uptime(struct admin_cmd_res* res, table_t* arg_tbl,
                             struct admin_conn_ctx* ctx) {
    snprintf(res->res, sizeof(res->res), "%lu", time(NULL)-ctx->lctx->start_time);
    res->ok = 1;
}

static void admin_cmd_sessions(struct admin_cmd_res* res, table_t* arg_tbl,
                               struct admin_conn_ctx* ctx) {
    int i = -1;
    struct vnc_session* vs = NULL;
    for(i = 0; i < ctx->lctx->vnc_sessions->len; i++) {
        vs = table_lookup_index(ctx->lctx->vnc_sessions, i);
        dprintf(ctx->fd, "session: %s %lu %s\r\n", vs->dst_host, vs->start_time, vs->host);
    }
    dprintf(ctx->fd, "sessions: count %u\r\n", ctx->lctx->vnc_sessions->len);
    res->ok = 1;
}

static void admin_cmd_add_host(struct admin_cmd_res* res, table_t* arg_tbl,
                               struct admin_conn_ctx* ctx) {
    struct db_vnc_ses_model vmodel;
    char ipbuf[INET6_ADDRSTRLEN];
    strncpy(vmodel.dst_addr,
            ((union admin_cmd_argv*)table_lookup_key(arg_tbl, "addr"))->s_val,
            sizeof(vmodel.dst_addr));
    vmodel.dst_port = ((union admin_cmd_argv*)table_lookup_key(arg_tbl, "port"))->i_val;
    strncpy(vmodel.vnc_pwd,
            ((union admin_cmd_argv*)table_lookup_key(arg_tbl, "password"))->s_val,
            sizeof(vmodel.vnc_pwd));
    vmodel.single_use = ((union admin_cmd_argv*)table_lookup_key(arg_tbl, "single_use")) >= 0 ? 1 : 0;
    vmodel.expiry = ((union admin_cmd_argv*)table_lookup_key(arg_tbl, "expiration"))->i_val;
    res->ok = db_insert_vses_entry(ctx->lctx->dbc, &vmodel);
    if(!res->ok) {
        res->err = "failed to insert entry";
        return;
    } else if(res->ok == 2) {
        res->ok = 0;
        res->err = "host already exists with the same password";
        return;
    }
    if(!inet_ntop(ctx->sockaddr.af,
                  NET_SOCKADDR_GET_SADDR(ctx->sockaddr),
                  ipbuf, sizeof(ipbuf)))
        return;
    logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "admin",
               "new host, %u (%s:%u), added by %s",
               vmodel.id,
               vmodel.dst_addr,
               vmodel.dst_port, ipbuf);
    snprintf(res->res, sizeof(res->res), "OK %u", vmodel.id);
}

static void admin_cmd_del_host(struct admin_cmd_res* res, table_t* arg_tbl,
                               struct admin_conn_ctx* ctx) {
    char ipbuf[INET6_ADDRSTRLEN];
    short ok = -1;
    uint32_t ses_id = ((union admin_cmd_argv*)table_lookup_key(arg_tbl, "id"))->i_val;
    ok = db_del_vses(ctx->lctx->dbc, ses_id);
    if(ok) {
        if(!inet_ntop(ctx->sockaddr.af,
                      NET_SOCKADDR_GET_SADDR(ctx->sockaddr),
                      ipbuf, sizeof(ipbuf)))
            return;
        logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "admin",
                   "host %u was removed by %s",
                   ses_id,
                   ipbuf);
        snprintf(res->res, sizeof(res->res), "OK %u", ses_id);
    }
    res->ok = ok;
}

static void admin_cmd_hosts(struct admin_cmd_res* res, table_t* arg_tbl,
                            struct admin_conn_ctx* ctx) {
    array_t* sessions = NULL;
    struct db_vnc_ses_model* session = NULL;
    int i = -1;
    if(!(sessions = db_lookup_all_vnc_sessions(ctx->lctx->dbc))) {
        res->err = "failed to query DB";
        res->ok = 0;
        return;
    }
    for(i = 0; i < sessions->len; i++) {
        session = sessions->items[i];
        dprintf(ctx->fd, "host: %u %s %u %s %u %lu\r\n", session->id,
                session->dst_addr,
                session->dst_port,
                session->vnc_pwd,
                session->single_use,
                session->expiry);
    }
    res->ok = 1;
    dprintf(ctx->fd, "hosts: count %u\r\n", sessions->len);
    array_free(sessions, 1);
}

static void admin_cmd_pass(struct admin_cmd_res* res, table_t* arg_tbl,
                           struct admin_conn_ctx* ctx) {
    char* password = NULL, ipbuf[INET6_ADDRSTRLEN];
    password = ((union admin_cmd_argv*)table_lookup_key(arg_tbl, "pass"))->s_val;
    assert(password != NULL);
    if(!inet_ntop(ctx->sockaddr.af,
                  NET_SOCKADDR_GET_SADDR(ctx->sockaddr),
                  ipbuf, sizeof(ipbuf)))
        return;
    if(!strcmp(password, ctx->lctx->admin_password)) {
        logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "admin",
                   "login from %s", ipbuf);
        strcpy(res->res, "you are now authorised");
        ctx->is_authed = 1;
    } else {
        logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "admin",
                   "failed login attempt from %s", ipbuf);
        res->err = "incorrect password";
        return;
    }
    res->ok = 1;
}

short admin_cmd_parse_args(char* buf, char** sp,
                           table_t* args, struct admin_cmd* cmd, char** err_arg) {
    struct admin_cmd_arg* carg = NULL;
    char* s = NULL;
    union admin_cmd_argv* argv = NULL;

    for(carg = cmd->args; carg->name; carg++) {
        if(!(s = strtok_r(NULL, " ", sp)) && carg->required) {
            *err_arg = carg->name;
            return 0;
        }
        else if(!s)
            // parameters are not named in any way, it's not possible to determine what it should be, anyway
            break;
        argv = malloc(sizeof(union admin_cmd_argv));
        switch(carg->type) {
        case ACMD_ARG_INT:
            if(atoi(s) < 0) {
                free(argv);
                *err_arg = carg->name;
                return 0;
            }
            argv->i_val = atoi(s);
            break;
        case ACMD_ARG_STR:
            argv->s_val = strdup(s);
            break;
        }
        table_insert(args, carg->name, argv);
    }
    return 1;
}

static void admin_cmd_free_argv(table_t* args, struct admin_cmd* cmd) {
    struct admin_cmd_arg* arg = NULL;
    union admin_cmd_argv* argv = NULL;
    for(arg = cmd->args; arg->name; arg++) {
        argv = table_lookup_key(args, arg->name);
        if(!argv || arg->type != ACMD_ARG_STR)
            continue;
        free(argv->s_val);
    }
    table_free(args, 1);
}

void admin_ln_recv_cb(struct evloop_fd_ev* ev,
                      char* obuf, int olen, struct admin_ln_ctx* ctx) {
    struct admin_conn_ctx* cctx = NULL;
    struct sockaddr* addr = NULL;
    int fd = -1;
    socklen_t salen = -1;
    if(!(cctx = malloc(sizeof(struct admin_conn_ctx))))
        return;
    cctx->sockaddr.af = ctx->addr_family;
    salen = NET_SOCKADDR_LEN(cctx->sockaddr);
    addr = NET_SOCKADDR_GET_ADDR(cctx->sockaddr);
    cctx->is_authed = 0;
    cctx->lctx = ctx;
    fd = accept(ev->fd, addr, &salen);
    if(fd < 0) // TODO: error handling
        return;
    cctx->fd = fd;
    send(fd, CONN_BANNER, sizeof(CONN_BANNER), 0);
    evloop_new_fd_ev(ctx->evctx, NULL, NULL,
                     (void*)admin_conn_recv_cb, -1, cctx, fd);
}

void admin_conn_recv_cb(struct evloop_fd_ev* ev, char* obuf,
                        int olen, struct admin_conn_ctx* ctx) {
    char buf[320+1], sbuf[320+1], *sp = NULL, *s = NULL;
    struct admin_cmd* cmd = NULL;
    struct admin_cmd_res cmd_res;
    table_t* arg_tbl = NULL;
    if(!cutil_read_line(ev->fd, buf, sizeof(buf))) {
        evloop_del_fd_ev(ctx->lctx->evctx, ev->fd);
        return;
    }
    memcpy(sbuf, buf, sizeof(buf));
    if(!(s = strtok_r(buf, " ", &sp)))
        return;
    for(cmd = admin_cmds; cmd->name; cmd++) {
        if(!strcmp(s, cmd->name)) {
            break;
        }
    }
    if(!cmd->name) {
        dprintf(ev->fd, "ERROR: %s\r\n", CMD_NOT_FOUND_ERR);
        return;
    }
    if(cmd->auth_required && !ctx->is_authed) {
        dprintf(ev->fd, "ERROR: %s\r\n", CMD_AUTH_REQ_ERR);
        return;
    }
    arg_tbl = table_new();
    if(cmd->args) {
        if(!admin_cmd_parse_args(buf, &sp, arg_tbl, cmd, &s)) {
            snprintf(buf, sizeof(buf)-1, CMD_ARG_ERR, s);
            dprintf(ev->fd, "ERROR: %s\r\n", buf);
            return;
        }
    }
    cmd_res.ok = 0;
    cmd_res.err = NULL;
    memset(cmd_res.res, 0, sizeof(cmd_res.res));
    assert(cmd->callback != NULL);
    cmd->callback(&cmd_res, arg_tbl, ctx);
    if(cmd->args)
        admin_cmd_free_argv(arg_tbl, cmd);
    if(cmd_res.ok && strlen(cmd_res.res) > 0) {
        dprintf(ev->fd, "%s: %s\r\n", cmd->name, cmd_res.res);
    } else if(!cmd_res.ok && cmd_res.err) {
        dprintf(ev->fd, "ERROR: %s: %s\r\n", cmd->name, cmd_res.err);
    }
}
