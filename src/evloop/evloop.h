#include <libcutil/array.h>
#include <libcutil/table.h>
#ifndef H_EVLOOP
#define H_EVLOOP
struct evloop_timer_ev {
    // EVL_TYPE_TIMER
    int end_time;
    void (*comp_cb)(struct evloop_timer_ev*, void*);
    void* opaque;
};
struct evloop_fd_ev {
    int fd;
    int buf_size;
    short _connected;
    void (*connect_cb)(struct evloop_fd_ev*, void*);
    void (*disconnect_cb)(struct evloop_fd_ev*, void*);
    void (*recv_cb)(struct evloop_fd_ev*, char*, int, void*);
    void* opaque;
};
struct evloop_ctx {
    array_t* evs;
    table_t* fevs;
};
struct evloop_ctx* evloop_new_ctx();
void evloop_new_timer_ev(struct evloop_ctx* ctx,
                         void(*comp_cb)(struct evloop_timer_ev*, void*),
                         void* opaque,
                         unsigned int secs);
void evloop_new_fd_ev(struct evloop_ctx* ctx,
                      void(*connect_cb)(struct evloop_fd_ev*, void*),
                      void(*disconnect_cb)(struct evloop_fd_ev*, void*),
                      void(*recv_cb)(struct evloop_fd_ev*, char*, int, void*),
                      int buf_size, void* opaque, int fd);
void evloop_init();
void evloop_del_fd_ev(struct evloop_ctx* ctx, int fd);
void evloop_run(struct evloop_ctx* ctx);
#endif
