#include <stdlib.h>
#include <time.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <signal.h>
#include <libcutil/table.h>
#include <assert.h>
#include "evloop.h"

#define POLL_MEVS 50
#define POLL_TIMEOUT 50

static void sigact_handler() {}

static void evloop_rpoll(struct pollfd* pevs, table_t* evs) {
    int i = -1, n = -1, rsize = -1;
    char* buf = NULL;
    socklen_t opt_len = sizeof(int);
    struct evloop_fd_ev* ev = NULL;
    for(i = 0; i < POLL_MEVS; i++) {
        if(!(ev = table_lookup_key(evs, &pevs[i-1].fd)))
            continue;
        if(pevs[i-1].revents & POLLOUT) {
            if(getsockopt(pevs[i-1].fd, SOL_SOCKET,
                          SO_ERROR, &n, &opt_len) == 0 && n != 0) {
                if(ev->disconnect_cb)
                    ev->disconnect_cb(ev, ev->opaque);
                table_del(evs, &pevs[i-1].fd);
                continue;
            }
            if(ev->connect_cb)
                ev->connect_cb(ev, ev->opaque);
            ev->_connected = 1;
        } else if(pevs[i-1].revents & POLLIN) {
            buf = NULL, rsize = -1;
            if(ev->buf_size >= 0) {
                if(!(buf = malloc(ev->buf_size)))
                    continue;
                memset(buf, 0, ev->buf_size);
                if((rsize = recv(pevs[i-1].fd, buf, ev->buf_size, 0)) < 1) {
                    if(ev->disconnect_cb)
                        ev->disconnect_cb(ev, ev->opaque);
                    free(buf);
                    table_del(evs, &pevs[i-1].fd);
                    continue;
                }
            }
            if(ev->recv_cb)
                ev->recv_cb(ev, buf, rsize, ev->opaque);
        }
    }
}

static void evloop_poll(struct evloop_ctx* ctx) {
    struct pollfd pfds[POLL_MEVS] = {0};
    struct evloop_fd_ev* ev = NULL;
    unsigned int i = 0, c = 0, d = 0;
    for(i = 0; i < ctx->fevs->len; i++) {
        ev = table_lookup_index(ctx->fevs, i);
        pfds[c].events = POLLIN;
        if(!ev->_connected)
            pfds[c].events |= POLLOUT;
        pfds[c].fd = ev->fd;
        c++;
        if(c >= POLL_MEVS) {
            if((d = poll(pfds, c, POLL_TIMEOUT)) > 0)
                evloop_rpoll(pfds, ctx->fevs);
            c = 0;
        }
    }
    if(c) {
        if((d = poll(pfds, c, POLL_TIMEOUT)) > 0)
            evloop_rpoll(pfds, ctx->fevs);
    }
}

void evloop_new_timer_ev(struct evloop_ctx* ctx,
                         void(*comp_cb)(struct evloop_timer_ev*, void*),
                         void* opaque,
                         unsigned int secs) {
    struct evloop_timer_ev* ev = malloc(sizeof(struct evloop_timer_ev));
    if(!ev)
        return;
    ev->end_time = time(NULL)+secs;
    ev->comp_cb = comp_cb;
    ev->opaque = opaque;
    assert(comp_cb != NULL);
    array_insert(ctx->evs, ev);
}

void evloop_new_fd_ev(struct evloop_ctx* ctx,
                      void(*connect_cb)(struct evloop_fd_ev*, void*),
                      void(*disconnect_cb)(struct evloop_fd_ev*, void*),
                      void(*recv_cb)(struct evloop_fd_ev*, char*, int, void*),
                      int buf_size, void* opaque, int fd) {
    struct evloop_fd_ev* ev = malloc(sizeof(struct evloop_fd_ev));
    if(!ev)
        return;
    ev->fd = fd;
    ev->buf_size = buf_size;
    ev->_connected = 0;
    if(!connect_cb)
        ev->_connected = 1;
    ev->connect_cb = connect_cb;
    ev->disconnect_cb = disconnect_cb;
    ev->recv_cb = recv_cb;
    ev->opaque = opaque;
    table_insert(ctx->fevs, &fd, ev);
}

void evloop_del_fd_ev(struct evloop_ctx* ctx, int fd) {
    struct evloop_fd_ev* ev = NULL;
    if((ev = table_lookup_key(ctx->fevs, &fd)))
        free(ev);
    table_del(ctx->fevs, &fd);
    close(fd);
}

struct evloop_ctx* evloop_new_ctx() {
    struct evloop_ctx* ctx = malloc(sizeof(struct evloop_ctx));
    if(!ctx)
        return NULL;
    ctx->fevs = table_new();
    ctx->evs = array_new();
    return ctx;
}

void evloop_init() {
    struct sigaction act = {0};
    act.sa_sigaction = sigact_handler;
    sigaction(SIGPIPE, &act, NULL);
}

void evloop_run(struct evloop_ctx* ctx) {
    int i = -1;
    struct evloop_timer_ev* ev = NULL;
    for(;;) {
        evloop_poll(ctx);
        for(i = 0; i < ctx->evs->len; i++) {
            ev = ctx->evs->items[i];
            if(time(NULL) >= ev->end_time)
                ev->comp_cb(ev, ev->opaque);
        }
    }
}
