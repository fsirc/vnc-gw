#include <arpa/inet.h>
#include <stdio.h>
#include "util.h"

void util_bhtonl(uint32_t in, char* out) {
    out[0] = in >> 24;
    out[1] = in >> 16;
    out[2] = in >> 8;
    out[3] = in & 0xFF;
}

void util_bhtons(uint16_t in, char* out) {
    out[0] = in >> 16;
    out[3] = in & 0xFF;
}

uint32_t util_bntohl(char* in) {
//    return (in[0] & 0xFF) | in[1] >> 8 | in[2] >> 16 | in[3] >> 24;
    return in[0] >> 24 | in[1] >> 16 | in[2] >> 8 | (in[3] & 0xFF);
}
