#include <stdint.h>
#include <sqlite3.h>
#include <libcutil/logger.h>
#include "evloop/evloop.h"
#include "net.h"
#ifndef H_ADMIN
#define H_ADMIN
enum admin_cmd_arg_type {
    ACMD_ARG_STR = 0,
    ACMD_ARG_INT = 1
};
struct admin_ln_ctx {
    struct evloop_ctx* evctx;
    logger_t* logger;
    sqlite3* dbc;
    table_t* vnc_sessions;
    char* admin_password;
    uint64_t start_time;
    int addr_family;
};
struct admin_conn_ctx {
    struct admin_ln_ctx* lctx;
    struct net_sockaddr sockaddr;
    short is_authed;
    int fd;
};
struct admin_cmd_res {
    short ok;
    char* err;
    char res[256+1];
};
struct admin_cmd_arg {
    char* name;
    short required;
    enum admin_cmd_arg_type type;
};
union admin_cmd_argv {
    char* s_val;
    int i_val;
};
struct admin_cmd {
    char* name;
    struct admin_cmd_arg* args;
    int auth_required;
    void(*callback)(struct admin_cmd_res*, table_t*, struct admin_conn_ctx*);
};
void admin_ln_recv_cb(struct evloop_fd_ev* ev,
                      char* obuf, int olen, struct admin_ln_ctx* ctx);
#endif
