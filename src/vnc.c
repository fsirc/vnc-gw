#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <assert.h>
#include <errno.h>
#include "des/d3des.h"
#include "db.h"
#include "vnc.h"
#include "net.h"
#include "des/d3des.h"
#include "util.h"

#define VNC_BUF_SIZE (640*480)
#define VNC_BANNER "RFB 003.008"
#define VNC_SYS_NAME "VNCGateway - %s"
#define VNC_SEC_ERR "Your client is at fault, please dilate"
#define VNC_SEC_AUTH_ERR "The password is either incorrect or the host is down"

struct vnc_session* vnc_new_session(char* dst_host,
                                    struct net_sockaddr sa) {
    struct vnc_session* vs = NULL;
    if(!(vs = malloc(sizeof(struct vnc_session))))
        return NULL;
    if(!inet_ntop(sa.af,
                  NET_SOCKADDR_GET_SADDR(sa), vs->host,
                  sizeof(vs->host)))
        return NULL;
    vs->start_time = time(NULL);
    strncpy(vs->dst_host, dst_host, sizeof(vs->dst_host));
    return vs;
}

int vnc_sec_handshake(char* buf) {
    buf[0] = 0x1;
    buf[1] = 0x2;
    return 2;
}

int vnc_sec_auth_challenge(char* pwd_challenge) {
    int i = -1;
    for(i = 0; i < 16; i++)
        pwd_challenge[i] = rand() % 255;
    return 16;
}

int vnc_sec_auth_status(char* buf, short ok) {
    util_bhtonl((ok ? 0 : 1), buf);
    return 4;
}

int vnc_sec_auth_err(char* buf) {
    util_bhtonl(sizeof(VNC_SEC_AUTH_ERR), buf);
    memcpy(buf+4, VNC_SEC_AUTH_ERR, sizeof(VNC_SEC_AUTH_ERR));
    return 4+sizeof(VNC_SEC_AUTH_ERR);
}

int vnc_sec_handshake_err(char* buf) {
    util_bhtonl(sizeof(VNC_SEC_ERR), buf);
    memcpy(buf+4, VNC_SEC_ERR, sizeof(VNC_SEC_ERR));
    return 4+sizeof(VNC_SEC_ERR);
}

int vnc_conn_validate_event(int recv_len, char* buf) {
    switch(buf[0]) {
    case EV_SET_ENCODINGS:
        if(recv_len < 1+1+2+4) return 0;
        break;
    case EV_FBR_UPDATE_REQ:
        if(recv_len < 1+1+2+2+2+2) return 0;
        break;
    case EV_KEY_EV:
        if(recv_len < 1+1+2+4) return 0;
        break;
    case EV_PTR_EV:
        if(recv_len < 1+1+2+2) return 0;
        break;
    case EV_C_CUT_TXT:
        if(recv_len > 1+3+4+1 || recv_len < 1+3+4) return 0;
        if(util_bntohl(&buf[4]) > recv_len-(1+3+4)) return 0;
        break;
    }
    return recv_len;
}

void vnc_ln_recv_cb(struct evloop_fd_ev* ev, char* obuf, int olen, struct vnc_ln_ctx* ctx) {
    int fd = -1;
    socklen_t salen = -1;
    struct vnc_conn_ctx* cctx = NULL;
    struct sockaddr* addr = NULL;
    char lbuf[INET6_ADDRSTRLEN];
    if(!(cctx = malloc(sizeof(struct vnc_conn_ctx)))) // TODO: error handling
        return;
    cctx->sockaddr.af = ctx->addr_family;
    salen = NET_SOCKADDR_LEN(cctx->sockaddr);
    addr = NET_SOCKADDR_GET_ADDR(cctx->sockaddr);
    fd = accept(ev->fd, addr, &salen);
    if(fd < 0) // TODO: error handling
        return;
    dprintf(fd, "%s\n", VNC_BANNER);
    cctx->state = VC_STATE_VBANNER;
    cctx->vhost = NULL;
    cctx->tctx = NULL;
    cctx->lctx = ctx;
    cctx->sockaddr.af = ctx->addr_family;
    if(!inet_ntop(ctx->addr_family, NET_SOCKADDR_GET_SADDR(cctx->sockaddr),
                  lbuf, sizeof(lbuf)))
        return;
    logger_log(ctx->logger, LOG_LEVEL_INFO, "vnc", "new connection from %s", lbuf);
    evloop_new_fd_ev(ctx->evctx, NULL, NULL, (void*)vnc_conn_recv_cb, -1, cctx, fd);
}

void vnc_tconn_connect_cb(struct evloop_fd_ev* ev, struct vnc_target_conn_ctx* ctx) {
    dprintf(ev->fd, "%s\n", VNC_BANNER);
}

void vnc_tconn_disconnect_cb(struct evloop_fd_ev* ev, struct vnc_target_conn_ctx* ctx) {
    if(ctx->state != TVC_STATE_PROXY)
        logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "vnc",
                   "failed to establish session with destination host %s for %s",
                   ctx->vses->dst_host, ctx->vses->host);
    table_del(ctx->lctx->vnc_sessions, &ctx->client_fd);
    evloop_del_fd_ev(ctx->lctx->evctx, ctx->client_fd);
    evloop_del_fd_ev(ctx->lctx->evctx, ev->fd);
}

void vnc_tconn_recv_cb(struct evloop_fd_ev* ev, char* obuf,
                       int olen, struct vnc_target_conn_ctx* ctx) {
    char buf[VNC_BUF_SIZE], challenge[16] = {0}, dname[256] = {0};
    int c_close = 0, o = -1;
    switch(ctx->state) {
    case VC_STATE_VBANNER:
        if(recv(ev->fd, buf, 12, 0) < 12) {
            c_close = 1;
            break;
        }
        ctx->state = VC_STATE_SH;
        break;
    case VC_STATE_SH:
        if(recv(ev->fd, buf, 2, 0) < 2 || buf[0] < 1 || !(buf[1] == 0x2 || buf[1] == 0x1)) {
            c_close = 1;
            break;
        }
        buf[0] = buf[1];
        send(ev->fd, buf, 1, 0);
        if(buf[0] == 0x2 && strlen(ctx->vnc_pwd) > 0) {
            ctx->state = TVC_STATE_PWD;
            break;
        }
        ctx->state = TVC_STATE_SHR;
        break;
    case TVC_STATE_PWD:
        if(recv(ev->fd, challenge, 16, 0) < 16) {
            c_close = 1;
            break;
        }
        deskey((void*)ctx->vnc_pwd, EN0);
        des((void*)challenge, (void*)buf);
        des((void*)challenge+8, (void*)buf+8);
        send(ev->fd, buf, 16, 0);
        ctx->state = TVC_STATE_SHR;
        break;
    case TVC_STATE_SHR:
        if(recv(ev->fd, buf, 4, 0) < 4 || util_bntohl(buf)) {
            c_close = 1;
            break;
        }
        // TODO: shared flag
        buf[0] = 0x0;
        send(ev->fd, buf, 1, 0);
        ctx->state = TVC_STATE_INIT;
        break;
    case TVC_STATE_INIT:
        if(recv(ev->fd, buf, 24, 0) < 24 || (o = util_bntohl(&buf[20])) > 256-1 ||
                recv(ev->fd, buf+24, o, 0) < o) {
            c_close = 1;
            break;
        }
        strncpy(dname, &buf[24], o);
        o = snprintf(&buf[24], 24+o, VNC_SYS_NAME, dname);
        util_bhtonl(o, buf+20);
        send(ctx->client_fd, buf, 24+o, 0);
        ctx->state = TVC_STATE_PROXY;
        break;
    case TVC_STATE_PROXY:
        o = recv(ev->fd, buf, VNC_BUF_SIZE, 0);
        if(o < 1) {
            c_close = 1;
            break;
        }
        send(ctx->client_fd, buf, o, 0);
        break;
    }
    if(c_close) {
        logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "vnc", "session closed by remote host %s for %s",
                   ctx->vses->dst_host, ctx->vses->host);
        table_del(ctx->lctx->vnc_sessions, &ctx->client_fd);
        evloop_del_fd_ev(ctx->lctx->evctx, ctx->client_fd);
        evloop_del_fd_ev(ctx->lctx->evctx, ev->fd);
    }
}

void vnc_conn_recv_cb(struct evloop_fd_ev* ev, char* obuf,
                      int olen, struct vnc_conn_ctx* ctx) {
    char buf[VNC_BUF_SIZE];
    struct db_vnc_ses_model* vhost = NULL;
    struct vnc_target_conn_ctx* tconn_ctx = NULL;
    struct vnc_session* vses = NULL;
    int c_close = 0, o = -1;
    switch(ctx->state) {
    case VC_STATE_VBANNER:
        if(recv(ev->fd, buf, 12, 0) < 12) {
            c_close = 1;
            break;
        }
        buf[12-1] = '\0';
        /*if(!strcmp(buf, VNC_BANNER)) {
          c_close = 1;
          break;
          }*/
        ctx->state = VC_STATE_SH;
        o = vnc_sec_handshake(buf);
        send(ev->fd, buf, o, 0);
        break;
    case VC_STATE_SH:
        o = recv(ev->fd, buf, 1, 0);
        if(!o) {
            c_close = 1;
            break;
        }
        if(buf[0] != 0x2) {
            o = vnc_sec_handshake_err(buf);
            send(ev->fd, buf, o, 0);
            c_close = 1;
            break;
        }
        o = vnc_sec_auth_challenge(ctx->pwd_challenge);
        send(ev->fd, ctx->pwd_challenge, o, 0);
        ctx->state = VC_STATE_PWD;
        break;
    case VC_STATE_PWD:
        if(recv(ev->fd, buf, 16, 0) < 16) {
            c_close = 1;
            break;
        }
        vhost = db_find_vses_entry(ctx->lctx->dbc, ctx->pwd_challenge, buf);
        o = vnc_sec_auth_status(buf, (vhost ? 1 : 0));
        send(ev->fd, buf, o, 0);
        if(vhost) {
            ctx->vhost = vhost;
        } else {
            o = vnc_sec_auth_err(buf);
            send(ev->fd, buf, o, 0);
            c_close = 1;
            break;
        }
        ctx->state = VC_STATE_INIT;
        break;
    case VC_STATE_INIT:
        // we don't care about the shared desktop flag >.<
        if(recv(ev->fd, buf, 1, 0) < 1 ||
                (ctx->host_fd = net_new_sock(ctx->vhost->dst_addr,
                                             ctx->vhost->dst_port)) < 0) {
            c_close = 1;
            break;
        }
        tconn_ctx = malloc(sizeof(struct vnc_target_conn_ctx));
        tconn_ctx->lctx = ctx->lctx;
        tconn_ctx->state = TVC_STATE_VBANNER;
        strncpy(tconn_ctx->vnc_pwd, ctx->vhost->vnc_pwd, 8);
        tconn_ctx->client_fd = ev->fd;
        // hold on, gotta do the session :D
        vses = vnc_new_session(ctx->vhost->dst_addr, ctx->sockaddr);
        tconn_ctx->vses = vses;
        table_insert(ctx->lctx->vnc_sessions, &ev->fd, vses);
        logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "vnc", "%s started for %s:%d by %s",
                   (ctx->vhost->single_use ? "single-use session" : "session"),
                   ctx->vhost->dst_addr, ctx->vhost->dst_port, vses->host);
        evloop_new_fd_ev(ctx->lctx->evctx,
                         (void*)vnc_tconn_connect_cb, (void*)vnc_tconn_disconnect_cb,
                         (void*)vnc_tconn_recv_cb, -1, tconn_ctx, ctx->host_fd);
        if(ctx->vhost->single_use)
            db_del_vses(ctx->lctx->dbc, ctx->vhost->id);
        ctx->tctx = tconn_ctx;
        ctx->state = VC_STATE_PROXY;
        break;
    case VC_STATE_PROXY:
        if(ctx->tctx->state == TVC_STATE_PROXY) {
            o = recv(ev->fd, buf, VNC_BUF_SIZE, 0);
            if(o < 1) {
                // client connection closed
                c_close = 1;
                evloop_del_fd_ev(ctx->lctx->evctx, ctx->host_fd);
                break;
            }
            o = vnc_conn_validate_event(o, buf);
            if(o < 1)
                break;
            send(ctx->host_fd, buf, o, 0);
        }
        break;
    }
    if(c_close) {
        if(ctx->vhost && ctx->tctx->vses)
            logger_log(ctx->lctx->logger, LOG_LEVEL_INFO, "vnc", "session closed for %s:%d by %s",
                       ctx->vhost->dst_addr, ctx->vhost->dst_port, ctx->tctx->vses->host);
        table_del(ctx->lctx->vnc_sessions, &ev->fd);
        evloop_del_fd_ev(ctx->lctx->evctx, ev->fd);
    }
}
