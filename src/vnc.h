#include <sqlite3.h>
#include <time.h>
#include <netinet/in.h>
#include <libcutil/logger.h>
#include "net.h"
#include "evloop/evloop.h"
#ifndef H_VNC
#define H_VNC
enum vnc_conn_state {
    // version banner
    VC_STATE_VBANNER = 0,
    // security handshake
    VC_STATE_SH = 1,
    // password challenge
    VC_STATE_PWD = 2,
    // server and client init
    VC_STATE_INIT = 3,
    // mmm
    VC_STATE_PROXY = 4
};
enum vnc_tconn_state {
    // version banner
    TVC_STATE_VBANNER = 0,
    // security handshake
    TVC_STATE_SH = 1,
    // password challenge
    TVC_STATE_PWD = 2,
    // security handshake result
    TVC_STATE_SHR = 3,
    // server and client init
    TVC_STATE_INIT = 4,
    TVC_STATE_PROXY = 5

};
enum vnc_events {
    EV_SET_PIXEL_FMT = 0,
    EV_SET_ENCODINGS = 2,
    EV_FBR_UPDATE_REQ = 3,
    EV_KEY_EV = 4,
    EV_PTR_EV = 5,
    EV_C_CUT_TXT = 6
};
struct vnc_session {
    char host[100+1];
    char dst_host[100+1];
    time_t start_time;
};
struct vnc_ln_ctx {
    struct evloop_ctx* evctx;
    logger_t* logger;
    sqlite3* dbc;
    table_t* vnc_sessions;
    short addr_family;
};
struct vnc_conn_ctx {
    struct vnc_ln_ctx* lctx;
    struct db_vnc_ses_model* vhost;
    struct vnc_target_conn_ctx* tctx;
    struct net_sockaddr sockaddr;
    enum vnc_conn_state state;
    char pwd_challenge[16];
    // Target VNC host
    int host_fd;
};
struct vnc_target_conn_ctx {
    struct vnc_ln_ctx* lctx;
    struct vnc_session* vses;
    enum vnc_tconn_state state;
    char vnc_pwd[8+1];
    int client_fd;
};
void vnc_ln_recv_cb(struct evloop_fd_ev* ev, char* obuf, int olen, struct vnc_ln_ctx* ctx);
void vnc_conn_recv_cb(struct evloop_fd_ev* ev, char* obuf, int olen, struct vnc_conn_ctx* ctx);
struct vnc_session* vnc_new_session(char* dst_host, struct net_sockaddr sa);
#endif
