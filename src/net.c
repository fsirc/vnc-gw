#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "net.h"

int net_new_ln(char* bind_host, uint16_t port, int* af) {
    int fd = -1, o = 1;
    struct addrinfo hints = {0};
    struct addrinfo* ai = NULL;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if(getaddrinfo(bind_host, NULL, &hints, &ai) != 0)
        return -1;
    fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
    fcntl(fd, F_SETFL, O_NONBLOCK);
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &o, sizeof(int));
    if(af != NULL)
        *af = ai->ai_family;
    if(ai->ai_family == AF_INET)
        ((struct sockaddr_in*)ai->ai_addr)->sin_port = htons(port);
    else
        ((struct sockaddr_in6*)ai->ai_addr)->sin6_port = htons(port);
    if(bind(fd, ai->ai_addr, ai->ai_addrlen) != 0) {
        freeaddrinfo(ai);
        return -1;
    }
    listen(fd, 256);
    freeaddrinfo(ai);
    return fd;
}

int net_new_sock(char* addr, uint16_t port) {
    int fd = -1;
    struct addrinfo hints = {0};
    struct addrinfo* ai = NULL;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    if(getaddrinfo(addr, NULL, &hints, &ai) != 0)
        return -1;
    fd = socket(ai->ai_family, ai->ai_socktype, ai->ai_protocol);
    fcntl(fd, F_SETFL, O_NONBLOCK);
    ((struct sockaddr_in*)ai->ai_addr)->sin_port = htons(port);
    if(connect(fd, ((struct sockaddr*)ai->ai_addr), ai->ai_addrlen) && errno != EINPROGRESS) {
        freeaddrinfo(ai);
        return -1;
    }
    freeaddrinfo(ai);
    return fd;
}
