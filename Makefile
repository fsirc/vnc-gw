ifdef MAKE_STATIC
CFLAGS += -static -static-pie
endif
SRC_DIR = src
CFLAGS += -lcutil -lpthread $(shell pkg-config --cflags --libs sqlite3)
_FILES = gateway.h config.h net.h util.h vnc.h admin.h db.h evloop/evloop.h des/d3des.h 
FILES = $(patsubst %,$(SRC_DIR)/%,$(_FILES))
OBJ_FILES = $(patsubst %.h,%.o,$(FILES))
.PHONY: clean install debug

$(SRC_DIR)/%.o: %.c $(FILES)
	$(CC) -c -o $@ $< $(CFLAGS) 

gateway: $(OBJ_FILES)
	$(CC) -o $@ $^ $(CFLAGS) 
install:
	install -m 775 gateway /usr/bin
clean:
	rm -f gateway $(SRC_DIR)/*.o $(SRC_DIR)/evloop/*.o $(SRC_DIR)/des/*.o
all: gateway
debug: CFLAGS += -g -DDEBUG
debug: gateway
